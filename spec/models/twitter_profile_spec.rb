require 'spec_helper'

describe TwitterProfile do
  
  before :each do
    TwitterProfile.find_each do |t|
      t.destroy
    end
    @oauth = JSON.parse "{\"provider\":\"twitter\",\"uid\":\"9876752\",\"info\":{\"nickname\":\"macool\",\"name\":\"Mario Andr\\u00e9s\",\"location\":\"-4.004809,-79.204649\",\"image\":\"http://a0.twimg.com/profile_images/2794899144/e08f52f0a3c139a3e0c5481e43ffc2fd_normal.png\",\"description\":\"A misfit of the world.\",\"urls\":{\"Website\":\"http://macool.me\",\"Twitter\":\"http://twitter.com/macool\"}},\"credentials\":{\"token\":\"9876752-EO3HiyN5j9adyXIWoyoI9da2zyDClNXUlOOTPoZfZy\",\"secret\":\"3acvr8Okq9SZi7szjgKgLC1yi73FguDF6ef788tU\"},\"extra\":{\"access_token\":{\"token\":\"9876752-EO3HiyN5j9adyXIWoyoI9da2zyDClNXUlOOTPoZfZy\",\"secret\":\"3acvr8Okq9SZi7szjgKgLC1yi73FguDF6ef788tU\",\"consumer\":{\"key\":\"DIPGVlMrkzVpdWJuF6vydg\",\"secret\":\"buUmMID4fZc5ShhXvjTKDQrnIk06owJfNEK3iBd2SA\",\"options\":{\"signature_method\":\"HMAC-SHA1\",\"request_token_path\":\"/oauth/request_token\",\"authorize_path\":\"/oauth/authenticate\",\"access_token_path\":\"/oauth/access_token\",\"proxy\":null,\"scheme\":\"header\",\"http_method\":\"post\",\"oauth_version\":\"1.0\",\"site\":\"https://api.twitter.com\"},\"http\":{\"address\":\"api.twitter.com\",\"port\":443,\"curr_http_version\":\"1.1\",\"no_keepalive_server\":false,\"close_on_empty_response\":false,\"socket\":null,\"started\":false,\"open_timeout\":30,\"read_timeout\":30,\"continue_timeout\":null,\"debug_output\":null,\"use_ssl\":true,\"ssl_context\":{\"cert\":null,\"key\":null,\"client_ca\":null,\"ca_file\":null,\"ca_path\":null,\"timeout\":null,\"verify_mode\":0,\"verify_depth\":null,\"verify_callback\":null,\"options\":4095,\"cert_store\":null,\"extra_chain_cert\":null,\"client_cert_cb\":null,\"tmp_dh_callback\":null,\"session_id_context\":null,\"session_get_cb\":null,\"session_new_cb\":null,\"session_remove_cb\":null,\"servername_cb\":null},\"enable_post_connection_check\":true,\"compression\":null,\"sspi_enabled\":false,\"ssl_version\":null,\"key\":null,\"cert\":null,\"ca_file\":null,\"ca_path\":null,\"cert_store\":null,\"ciphers\":null,\"verify_mode\":0,\"verify_callback\":null,\"verify_depth\":null,\"ssl_timeout\":null},\"http_method\":\"post\",\"uri\":{\"scheme\":\"https\",\"user\":null,\"password\":null,\"host\":\"api.twitter.com\",\"port\":443,\"path\":\"\",\"query\":null,\"opaque\":null,\"registry\":null,\"fragment\":null,\"parser\":null}},\"params\":{\"oauth_token\":\"9876752-EO3HiyN5j9adyXIWoyoI9da2zyDClNXUlOOTPoZfZy\",\"oauth_token_secret\":\"3acvr8Okq9SZi7szjgKgLC1yi73FguDF6ef788tU\",\"user_id\":\"9876752\",\"screen_name\":\"macool\"},\"response\":{\"date\":[\"Mon, 11 Feb 2013 17:07:19 GMT\"],\"status\":[\"200 OK\"],\"x-frame-options\":[\"SAMEORIGIN\"],\"pragma\":[\"no-cache\"],\"expires\":[\"Tue, 31 Mar 1981 05:00:00 GMT\"],\"x-ratelimit-reset\":[\"1360606039\"],\"x-access-level\":[\"read-write\"],\"x-ratelimit-class\":[\"api\"],\"x-ratelimit-limit\":[\"150\"],\"content-length\":[\"1976\"],\"content-type\":[\"application/json; charset=utf-8\"],\"x-ratelimit-remaining\":[\"149\"],\"x-runtime\":[\"0.03913\"],\"x-transaction-mask\":[\"a6183ffa5f8ca943ff1b53b5644ef11401191374\"],\"etag\":[\"\\\"75bd0049292beb474f9630e330d97b18\\\"\"],\"last-modified\":[\"Mon, 11 Feb 2013 17:07:19 GMT\"],\"x-transaction\":[\"d9bcf87378062ea2\"],\"x-mid\":[\"58ce8de1938db947cd5500cea6c9219db91576b3\"],\"cache-control\":[\"no-cache, no-store, must-revalidate, pre-check=0, post-check=0\"],\"set-cookie\":[\"k=10.40.70.105.1360602439291422; path=/; expires=Mon, 18-Feb-13 17:07:19 GMT; domain=.twitter.com\",\"guest_id=v1%3A136060243931835773; domain=.twitter.com; path=/; expires=Thu, 12-Feb-2015 05:07:19 GMT\",\"dnt=; domain=.twitter.com; path=/; expires=Thu, 01-Jan-1970 00:00:00 GMT\",\"lang=es; path=/\",\"lang=es; path=/\",\"lang=es; path=/\",\"twid=u%3D9876752%7CFVaqQSG4dHn2A05dHFKFVKtuHlY%3D; domain=.twitter.com; path=/; secure\",\"_twitter_sess=BAh7CSIKZmxhc2hJQzonQWN0aW9uQ29udHJvbGxlcjo6Rmxhc2g6OkZsYXNo%250ASGFzaHsABjoKQHVzZWR7ADoPY3JlYXRlZF9hdGwrCKOeOco8AToMY3NyZl9p%250AZCIlN2UyMTMyYzgwOWE5YzcxNDNiMGY1NjNiZGQ5MGRlOTE6B2lkIiU3MWQ0%250AZWE2MWU4YzRlMTM0MzRkOTY4ZGI4NTNiODJlMQ%253D%253D--f1969c7eadee411c860b509d452949a35aa401a8; domain=.twitter.com; path=/; HttpOnly\"],\"vary\":[\"Accept-Encoding\"],\"server\":[\"tfe\"],\"connection\":[\"close\"]}},\"raw_info\":{\"id\":9876752,\"default_profile_image\":false,\"profile_background_image_url\":\"http://a0.twimg.com/profile_background_images/120462524/ubuntu.jpg\",\"friends_count\":589,\"favourites_count\":129,\"profile_link_color\":\"0084B4\",\"profile_background_image_url_https\":\"https://si0.twimg.com/profile_background_images/120462524/ubuntu.jpg\",\"is_translator\":false,\"utc_offset\":-18000,\"screen_name\":\"macool\",\"followers_count\":672,\"name\":\"Mario Andr\\u00e9s\",\"lang\":\"es\",\"profile_use_background_image\":true,\"created_at\":\"Fri Nov 02 05:47:16 +0000 2007\",\"profile_text_color\":\"333333\",\"notifications\":false,\"protected\":false,\"id_str\":\"9876752\",\"statuses_count\":9062,\"url\":\"http://macool.me\",\"contributors_enabled\":false,\"default_profile\":false,\"profile_sidebar_border_color\":\"C0DEED\",\"time_zone\":\"Quito\",\"profile_image_url_https\":\"https://si0.twimg.com/profile_images/2794899144/e08f52f0a3c139a3e0c5481e43ffc2fd_normal.png\",\"geo_enabled\":true,\"verified\":false,\"profile_image_url\":\"http://a0.twimg.com/profile_images/2794899144/e08f52f0a3c139a3e0c5481e43ffc2fd_normal.png\",\"following\":false,\"profile_background_tile\":true,\"listed_count\":20,\"profile_sidebar_fill_color\":\"DDEEF6\",\"location\":\"-4.004809,-79.204649\",\"status\":{\"in_reply_to_status_id_str\":null,\"geo\":{\"type\":\"Point\",\"coordinates\":[-3.970456,-79.204442]},\"truncated\":false,\"contributors\":null,\"place\":null,\"in_reply_to_user_id_str\":null,\"favorited\":false,\"coordinates\":{\"type\":\"Point\",\"coordinates\":[-79.204442,-3.970456]},\"id_str\":\"300516182707027968\",\"created_at\":\"Sun Feb 10 08:06:55 +0000 2013\",\"retweet_count\":0,\"text\":\"so never mind the darkness, we still can find a way. #DrunkTweets\",\"retweeted\":false,\"source\":\"<a href=\\\"http://www.apple.com\\\" rel=\\\"nofollow\\\">iOS</a>\",\"id\":300516182707027968,\"in_reply_to_screen_name\":null,\"in_reply_to_user_id\":null,\"in_reply_to_status_id\":null},\"follow_request_sent\":false,\"description\":\"A misfit of the world.\",\"profile_background_color\":\"C0DEED\"}}}"
  end

  it "should create valid twitter profile with attributes" do
    t = TwitterProfile.new( @oauth )
    t.user_id = 1
    t.save.should be_true
    TwitterProfile.count.should eql(1)
  end

  it "should not create two repeated users" do
    TwitterProfile.count.should eql(0)
    a = TwitterProfile.new @oauth
    b = TwitterProfile.new @oauth
    a.user_id = 2
    b.user_id = 3
    a.save.should be_true
    b.save.should be_false
  end

  it "should update twitter profile when exists" do
    t = TwitterProfile.create_or_update( @oauth )
    t.user_id = 4
    t.save.should be_true
    t.nickname.should eql("macool")
    @oauth["info"]["nickname"] = "marioandres"
    t = TwitterProfile.create_or_update( @oauth )
    t.nickname.should eql("marioandres")
    TwitterProfile.count.should eql(1)
  end

end
