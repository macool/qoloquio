require 'spec_helper'

describe FacebookProfile do

  before :each do
    FacebookProfile.find_each do |f|
      f.destroy
    end
    @json_attrs = JSON.parse "{\"provider\":\"facebook\",\"uid\":\"1388871348\",\"info\":{\"nickname\":\"mario.andres.correa\",\"email\":\"93.maco@gmail.com\",\"name\":\"Mario Andr\\u00e9s Correa\",\"first_name\":\"Mario\",\"last_name\":\"Correa\",\"image\":\"http://graph.facebook.com/1388871348/picture?type=square\",\"description\":\"A misfit of the world. Web developer - Ruby on Rails, PHP, MySQL, HTML5, Ajax, JQuery, CSS3, iOS Web Apps\",\"urls\":{\"Facebook\":\"http://www.facebook.com/mario.andres.correa\"},\"location\":\"Loja, Ecuador\",\"verified\":true},\"credentials\":{\"token\":\"AAAF4VGMbnAgBAB4siR3c9QRxgu78aXBYNC1ZALfaFTuatCMXjWupexzvHfcmEQZCXkgPaYrGcn2tfffLeZCFJzRGL22CPUm4Uxb7j8xsAZDZD\",\"expires_at\":1365637373,\"expires\":true},\"extra\":{\"raw_info\":{\"id\":\"1388871348\",\"name\":\"Mario Andr\\u00e9s Correa\",\"first_name\":\"Mario\",\"middle_name\":\"Andr\\u00e9s\",\"last_name\":\"Correa\",\"link\":\"http://www.facebook.com/mario.andres.correa\",\"username\":\"mario.andres.correa\",\"birthday\":\"05/10/1993\",\"hometown\":{\"id\":\"107623235927530\",\"name\":\"Loja, Ecuador\"},\"location\":{\"id\":\"107623235927530\",\"name\":\"Loja, Ecuador\"},\"bio\":\"A misfit of the world. Web developer - Ruby on Rails, PHP, MySQL, HTML5, Ajax, JQuery, CSS3, iOS Web Apps\",\"quotes\":\"\\\"Free as in freedom\\\"\\r\\n\\u200e\\\"La vida es como una caja de chocolates, excepto cuando los chocolates est\\u00e1n en un balde, entonces es como un balde de chocolates\\\". Bob Esponja\",\"work\":[{\"employer\":{\"id\":\"141818599287897\",\"name\":\"Caxanuma\"},\"location\":{\"id\":\"107623235927530\",\"name\":\"Loja, Ecuador\"},\"position\":{\"id\":\"140390929426530\",\"name\":\"CEO\"},\"start_date\":\"2011-10\"}],\"sports\":[{\"id\":\"108167272538953\",\"name\":\"Table tennis\",\"with\":[{\"id\":\"100000482315038\",\"name\":\"Carlos Javier Merino Peralta\"},{\"id\":\"100001323877217\",\"name\":\"Luis Antonio Ortega\"}],\"description\":\"Jugamos dindop.\"}],\"education\":[{\"school\":{\"id\":\"131783620225483\",\"name\":\"Tagesschule y Colegio\\\" San Gerardo\\\"\"},\"year\":{\"id\":\"142963519060927\",\"name\":\"2010\"},\"type\":\"High School\"},{\"school\":{\"id\":\"108083032558084\",\"name\":\"Universidad Tecnica Particular de Loja\"},\"year\":{\"id\":\"127342053975510\",\"name\":\"2016\"},\"type\":\"College\"},{\"school\":{\"id\":\"114948101851780\",\"name\":\"Universidad T\\u00e9cnica Particular de Loja UTPL\"},\"year\":{\"id\":\"127342053975510\",\"name\":\"2016\"},\"concentration\":[{\"id\":\"180975295273185\",\"name\":\"Electronica y Telecomunicaciones\"}],\"type\":\"College\"}],\"gender\":\"male\",\"email\":\"93.maco@gmail.com\",\"timezone\":-5,\"locale\":\"en_US\",\"languages\":[{\"id\":\"106059522759137\",\"name\":\"English\"},{\"id\":\"110343528993409\",\"name\":\"Spanish\"}],\"verified\":true,\"updated_time\":\"2012-12-20T17:48:14+0000\"}}}"
  end

  it "should create valid facebook profile with these json attributes" do
    f = FacebookProfile.new( @json_attrs )
    f.user_id = 1
    f.save.should be_true
    FacebookProfile.count.should eql(1)
  end

  it "should not create two repeated users" do
    FacebookProfile.count.should eql(0)
    f = FacebookProfile.new( @json_attrs )
    g = FacebookProfile.new( @json_attrs )
    f.user_id = 2
    g.user_id = 3
    f.save.should be_true
    g.save.should be_false
  end

  it "should update facebook profile when exists." do
    f = FacebookProfile.create_or_update( @json_attrs )
    f.user_id = 3
    f.save.should be_true
    f.last_name.should eql("Correa")
    @json_attrs["info"]["last_name"] = "macool"
    f = FacebookProfile.create_or_update( @json_attrs )
    f.last_name.should eql("macool")
    FacebookProfile.count.should eql(1)
  end

end
