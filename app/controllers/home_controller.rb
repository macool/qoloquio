# encoding: utf-8
class HomeController < ApplicationController

  def index
    if @preference = Preference.find_by_key("show_static_landing_page") and @preference.evalued_value[:show]
      render @preference.evalued_value[:page], layout: @preference.evalued_value[:show_layout]
    else
      if @video = Video.where("en_vivo_desde <= :time_now AND en_vivo_hasta >= :time_now", {:time_now => Time.now}).first
        @is_playing_content = true
        @is_live = true
        render "content_playing"
      else
        @videos = Video.where(:published => true).where("en_vivo_desde <= :time_now AND en_vivo_hasta <= :time_now", {:time_now => Time.now})
        render "already_played"
      end
    end
  end

  def auth_failure
    flash["alert-notice"] = "Ups, algo salió mal. Por favor vuelve a intentar"
    redirect_to root_path
  end

end
