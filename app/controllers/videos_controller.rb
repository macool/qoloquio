include ActionView::Helpers::DateHelper

class VideosController < ApplicationController

  def show
    if @video = Video.find_by_id_and_published(params[:id], true)
      render 'home/content_playing'
    else
      render_not_found
      return
    end
  end

  def tl
    segmentos = Segmento.where(video_id: params[:id]).includes(:segmento_urls)
    respond_to do |format|
      format.js {
        time = Time.now
        @segmentos_antes = []
        @segmentos_ahora = []
        @segmentos_despues = []
        segmentos.each do |segmento|
          if segmento.hasta < time
            @segmentos_antes.push segmento.prepare_for_js
          elsif segmento.desde <= time and segmento.hasta >= time
            @segmentos_ahora.push segmento.prepare_for_js
          elsif segmento.desde > time
            @segmentos_despues.push segmento.prepare_for_js
          end
        end
      }
      format.json {
        # segmentos_todos = []
        # segmentos.each do |segmento|
        #   if segmento.has_url?
        #     segmentos_todos.push segmento
        #   end
        # end
        segmentos = segmentos.where("segmento_urls_count > 0")
        segmentos = segmentos.map do |segmento|
          {
            :id => segmento.id,
            :urls => segmento.urls,
            :height => segmento.height,
            :width => segmento.width,
            :titulo => segmento.titulo,
            :duracion => segmento.duracion,
            :duracion_in_words => distance_of_time_in_words( segmento.duracion, 0, true )
          }
        end
        render :json => segmentos
      }
    end
  end

end
