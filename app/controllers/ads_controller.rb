class AdsController < ApplicationController

  respond_to :json

  def index
    ads = Ad.order("id ASC").where("desde <= :time_now and hasta >= :time_now", {:time_now => Time.now}).map do |ad|
      {
        :id => ad.id,
        :image => ad.image_url,
        :url => ad.url,
        :display_time => ad.display_time
      }
    end
    render :json => ads
  end

  def create
    if ad = Ad.find_by_id(params[:id])
      ad.times_displayed += 1
      ad.save
    end
    render :json => ""
  end

  def visited
    if ad = Ad.find_by_id(params[:id])
      ad.clicks += 1
      ad.save
    end
    render :json => ""
  end

end
