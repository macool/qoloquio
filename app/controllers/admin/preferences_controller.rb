# encoding: utf-8
class Admin::PreferencesController < AdminController
  
  before_filter :set_place

  def index
    @preferences = Preference.all
  end

  def new
    @preference = Preference.new
  end

  def create
    @preference = Preference.new params[:preference]
    if @preference.save
      flash["alert-success"] = "preferencia creada."
      redirect_to action: :index
    else
      flash["alert-error"] = "preferencia no creada."
      render :new
    end
  end

  def edit
    @preference = Preference.find params[:id]
    render :new
  end

  def update
    if @preference = Preference.update(params[:id], params[:preference])
      flash["alert-success"] = "preferencia actualizada."
      redirect_to action: :index
    else
      flash["alert-error"] = "preferencia no actualizada."
      render :new
    end
  end

  private

    def set_place
      @place = "preferences"      
    end

end