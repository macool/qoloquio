# encoding: utf-8
class Admin::AdsController < AdminController
  
  before_filter :set_place
  before_filter :get_users_mapped, :only => [:new, :edit, :create, :update]

  def index
    @ads = Ad.all(:include => :user)
  end

  def new
    @ad = Ad.new
    render :edit
  end

  def edit
    @ad = Ad.find params[:id]
  end

  def create
    @ad = Ad.new params[:ad]
    if @ad.save
      flash["alert-success"] = "anuncio creado."
      redirect_to action: :index
    else
      flash["alert-error"] = "algo salió mal."
      render :edit
    end
  end

  def update
    @ad = Ad.find params[:id]
    if @ad.update_attributes(params[:ad])
      flash["alert-success"] = "anuncio actualizado."
      redirect_to action: :index
    else
      flash["alert-error"] = "algo salió mal."
      render :edit
    end
  end


  private

    def set_place
      @place = "ads"
    end

    def get_users_mapped
      @users_mapped = User.all(include: [:facebook_profile, :twitter_profile]).map { |u| [u.default_profile_display, u.id] }
    end
  
end