class Admin::UsersController < AdminController
  
  before_filter :set_place

  def index
    @users = User.order("is_admin DESC, banned DESC").includes(:twitter_profile, :facebook_profile)
    @users = @users.where("the_name LIKE :name", {:name => "%#{params[:search_name]}%"}) unless params[:search_name].blank?
    @users = @users.page(params[:page]).per(10)
  end

  def ban
    @user = User.find(params[:id])
    @user.banned = params[:ban]
    @user.save
    render :user
  end

  def admin
    @user = User.find(params[:id])
    @user.is_admin = params[:be]
    @user.save
    render :user
  end

  private

    def set_place
      @place = "users"
    end
  
end