# encoding: utf-8
class Admin::VideosController < AdminController

  before_filter :set_place
  before_filter :find_video, only: [:edit, :update, :embed]

  def index
    @videos = Video.all
  end

  def new
    @video = Video.new
    render :edit
  end

  def edit
  end

  def embed
    @embed_url = app_url + embedded_path(@video)
  end

  def create
    @video = Video.new params[:video]
    if @video.save
      flash["alert-success"] = "programa creado."
      redirect_to action: :index
    else
      flash["alert-error"] = "algo salió mal."
      render :edit
    end
  end

  def update
    if @video.update_attributes(params[:video])
      flash["alert-success"] = "programa actualizado."
      redirect_to action: :index
    else
      flash["alert-error"] = "algo salió mal."
      render :edit
    end
  end

  def destroy
    @video = Video.find params[:id]
    @video.destroy
    redirect_to action: :index
  end

  private

  def find_video
    @video = Video.find params[:id]
  end

  def set_place
    @place = "programas"
  end
end