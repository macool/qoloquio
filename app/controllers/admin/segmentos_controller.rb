# encoding: utf-8
class Admin::SegmentosController < AdminController
  
  def edit
    @segmento = Segmento.find params[:id]
  end

  def update
    @segmento = Segmento.find params[:id]
    if @segmento.update_attributes params[:segmento]
      flash["alert-success"] = "segmento actualizado."
      redirect_to admin_videos_path
    else
      flash["alert-error"] = "algo salió mal."
      render :edit
    end
  end
  
end