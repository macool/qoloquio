class CommentsController < ApplicationController

  before_filter :confirm_logged_in, :only => :create

  def show
    comments = Comment.where("id > :since_id AND video_id = :video_id", { :since_id => params[:since_id].to_i, :video_id => params[:id].to_i }).includes(:user)
    @comments = []
    comments.find_each do |comment|
      @comments << comment.prepare_js_to_sidebar
    end
  end

  def create
    if current_user.banned
      render :banned
      return
    end
    params[:comment][:texto].strip!
    @comment = Comment.new params[:comment]
    @comment.user_id = current_user.id
    if @comment.save
      CommentsWorker.perform_async(@comment.id)
    else
      render :not_created
    end
  end

  def count
    render :text => Comment.where(:video_id => params[:id]).count
  end

end
