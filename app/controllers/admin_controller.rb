class AdminController < ApplicationController

  before_filter :confirm_logged_in_admin

  layout 'admin'

  def index
    
  end

  protected

    def confirm_logged_in_admin
      unless current_user and current_user.is_admin?
        # flash["alert-error"] = "creo que no puedes hacer eso."
        redirect_to root_path
        return
      end
    end

end
