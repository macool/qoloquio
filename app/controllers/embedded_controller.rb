require "uglifier"

class EmbeddedController < ApplicationController
  def show
    @video = Video.find params[:id]
    @app_url = app_url
    render js: Uglifier.compile(render_to_string)
  end
end
