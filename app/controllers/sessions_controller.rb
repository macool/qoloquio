# encoding: utf-8
class SessionsController < ApplicationController

  def destroy
    flash["alert-success"] = "cerraste tu sesión"
    session[:user_id] = nil
    redirect_to :back
  end

end
