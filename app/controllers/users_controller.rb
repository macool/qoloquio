# encoding: utf-8
class UsersController < ApplicationController

  def create
    user = User.create_or_update_from_omniauth(request.env["omniauth.auth"])
    session[:user_id] = user.id
    flash["alert-success"] = "iniciaste sesión como #{current_user.name}"
    redirect_to root_path
  end

end
