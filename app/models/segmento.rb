# encoding: utf-8
class Segmento < ActiveRecord::Base
  attr_accessible :titulo,
                  :video_id,
                  :segmento_urls_attributes,
                  :width,
                  :height,
                  :duracion

  # relations:
    belongs_to :video
    has_many :segmento_urls, :dependent => :destroy
    accepts_nested_attributes_for :segmento_urls, :allow_destroy => true, :reject_if => lambda { |s| s[:url].blank? }

  # validations:
    validates :video_id, :presence => true
    validates :titulo, :presence => true
    validates :desde, :presence => true
    validates :hasta, :presence => true
    validates :width, :numericality => {:greater_than => 0}
    validates :height, :numericality => {:greater_than => 0}
    validates :duracion, :presence => true, :numericality => {:greater_than => 0}

  # methods
    def desde
      @desde ||= self.video.segmento_desde( self )
    end
    def hasta
      @hasta ||= desde + self.duracion if desde and duracion
    end
    def url
      self.segmento_urls.first.url if self.segmento_urls.any?
    end
    def has_url?
      self.segmento_urls.any?
    end
    def urls
      self.segmento_urls.map do |url|
        url.url
      end
    end
    def prepare_for_js
      {
        :id => id,
        :urls => urls,
        :height => height,
        :width => width,
        :titulo => titulo,
        :duracion => duracion,
        :desde => I18n.l(desde, format: :q_hour),
        :hasta => I18n.l(hasta, format: :q_hour)
      }
    end

end
