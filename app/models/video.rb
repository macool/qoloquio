class Video < ActiveRecord::Base
  attr_accessible :poster,
                  :remove_poster,
                  :poster_cache,
                  :iframe_code,
                  :en_vivo_desde,
                  :en_vivo_hasta,
                  :titulo,
                  :descripcion,
                  :segmentos_attributes,
                  :published,
                  :hashtag,
                  :wowza_file_width,
                  :wowza_file_height,
                  :wowza_server,
                  :wowza_app,
                  :wowza_channel,
                  :kind

  mount_uploader :poster, VideoPosterUploader

  # validations:
    validates :titulo, :presence => true
    validate :desde_is_menor_a_hasta

  # relations:
    has_many :comments
    has_many :segmentos, :dependent => :destroy
    accepts_nested_attributes_for :segmentos, :allow_destroy => true

  # methods:
    def desde_is_menor_a_hasta
      unless self.en_vivo_desde < self.en_vivo_hasta
        errors.add :en_vivo_hasta, "debe ser mayor al tiempo en el que empieza"
      end
    end
    def segmento_desde( segmento )
      segmentos = self.segmentos.select([:id, :duracion])
      index = segmentos.index( segmento )
      time = self.en_vivo_desde
      if index and not index === 0
        (0..(index-1)).each do |i|
          time += segmentos[i].duracion.to_i
        end
      end
      time
    end
    def video?
      kind == "Video"
    end
    def audio?
      not video?
    end
    def wowza_file_url
      "rtmp://#{wowza_server}/#{wowza_app}/:#{wowza_channel}"
    end
    def html_url
      "http://#{wowza_server}:1935/#{wowza_app}/mp3:#{wowza_channel}/playlist.m3u8"
    end

end
