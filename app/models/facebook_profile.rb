class FacebookProfile < ActiveRecord::Base
  attr_accessible :uid,
                  # :nickname,
                  # :email,
                  # :name,
                  # :first_name,
                  # :last_name,
                  # :image,
                  # :urls,
                  # :location,
                  # :verified,
                  # :token,
                  # :token_expires_at,
                  # :token_expires,
                  # :raw_info,
                  # :user_id,
                  # :created_at,
                  # :updated_at,
                  # accessors:
                  :provider, :info, :credentials, :extra

  attr_accessor :provider, :info, :credentials, :extra
  lazy_load :raw_info

  before_validation :set_correct_attributes
  after_save :update_user

  # validations:
    validates :uid, :presence => true, :uniqueness => true
    validates :email, :presence => true, :uniqueness => true
    validates :token, :presence => true, :uniqueness => true
    validates :user_id, :presence => true, :uniqueness => true

  # attrs:
    serialize :raw_info
    serialize :urls

  # associations:
    belongs_to :user

  # methods:
    def set_correct_attributes
      self.nickname = self.info["nickname"]
      self.email = self.info["email"]
      self.name = self.info["name"]
      self.first_name = self.info["first_name"]
      self.last_name = self.info["last_name"]
      self.image = self.info["image"]
      self.urls = self.info["urls"]
      self.location = self.info["location"]
      self.verified = self.info["verified"]
      self.token = self.credentials["token"]
      self.token_expires_at = self.credentials["expires_at"]
      self.token_expires = self.credentials["expires"]
      self.raw_info = self.extra["raw_info"]
    end
    def share( obj )
      unless self.token.blank?
        graph = Koala::Facebook::API.new(self.token)
        begin
          graph.put_connections("me","feed",obj)
          return true
        rescue
          return false
        end
      end
    end
    def update_user
      self.user.save
    end

  # class methods:
    def self.create_or_update( attributes )
      if profile = self.find_by_uid( attributes["uid"] )
        profile.update_attributes attributes 
      else
        profile = FacebookProfile.new( attributes )
      end
      profile
    end

end
