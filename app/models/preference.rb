class Preference < ActiveRecord::Base
  
  attr_accessible :key,
                  :value,
                  :descripcion

  serialize :value

  # validations:
    validates :key, :presence => true,
                    :uniqueness => true
    validates :value, :presence => true

  # methods:
    def evalued_value
      @evalue_value ||= eval(self.value)
    end

end
