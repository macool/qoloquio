class TwitterProfile < ActiveRecord::Base
  attr_accessible :uid,
                  # :nickname,
                  # :name,
                  # :description,
                  # :location,
                  # :image,
                  # :urls,
                  # :token_access,
                  # :token_secret,
                  # :raw_info,
                  # :user_id
                  # attr acessors:
                  :provider, :info, :credentials, :extra

  attr_accessor :provider, :info, :credentials, :extra
  lazy_load :raw_info

  before_validation :set_correct_attributes
  after_save :update_user

  # validations:
    validates :uid, :presence => true, :uniqueness => true
    validates :token_access, :presence => true, :uniqueness => true
    validates :token_secret, :presence => true, :uniqueness => true
    validates :user_id, :presence => true, :uniqueness => true

  # attrs:
    serialize :raw_info
    serialize :urls

  # associations:
    belongs_to :user

  # methods:
    def set_correct_attributes
      self.nickname = self.info["nickname"]
      self.name = self.info["name"]
      self.description = self.info["description"]
      self.location = self.info["location"]
      self.image = self.info["image"]
      self.urls = self.info["urls"]
      self.token_access = self.credentials["token"]
      self.token_secret = self.credentials["secret"]
      self.raw_info = self.extra["raw_info"]
    end
    def share(obj)
      unless self.token_access.blank? and self.token_secret.blank?
        client = Twitter::REST::Client.new do |config|
          config.consumer_key        = OMNIAUTH_CREDENTIALS["twitter"]["access"]
          config.consumer_secret     = OMNIAUTH_CREDENTIALS["twitter"]["secret"]
          config.access_token        = self.token_access
          config.access_token_secret = self.token_secret
        end
        begin
          client.update( obj[:message] )
          return true
        rescue
          return false
        end
      end
    end
    def update_user
      self.user.save
    end

  # class methods:
    def self.create_or_update( attributes )
      if profile = self.find_by_uid( attributes["uid"] )
        profile.update_attributes attributes
      else
        profile = TwitterProfile.new( attributes )
      end
      profile
    end

end
