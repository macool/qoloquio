# encoding: utf-8
class Ad < ActiveRecord::Base
  attr_accessible :desde,
                  :hasta,
                  :image,
                  :remove_image,
                  :image_cache,
                  :user_id,
                  :nombre,
                  :url,
                  :display_time

  mount_uploader :image, AdImageUploader

  # validations:
    validates :desde, :presence => true
    validates :hasta, :presence => true
    validates :url, :presence => true
    # validates :image, :presence => true
    validates :nombre, :presence => true
    validates :user_id, :presence => true
    validates :display_time, :presence => true, :inclusion => {:in => 1..300}
    validate :desde_is_menor_a_hasta
    validate :url_has_protocol

  # relationships:
    belongs_to :user

  # methods:
    def url_has_protocol
      valid = false
      supported_protocols = [
        "http://",
        "https://"
      ]
      supported_protocols.each do |protocol|
        if self.url.include?(protocol)
          valid = true
        end
      end
      unless valid
        errors.add :url, "actualmente están soportados solamente los protocolos: #{supported_protocols.join(", ")}"
      end
    end
    def desde_is_menor_a_hasta
      unless self.desde < self.hasta
        errors.add :hasta, "debe ser mayor al tiempo en el que empieza"
      end
    end

end
