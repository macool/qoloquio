class User < ActiveRecord::Base
  attr_accessible :profile

  # relations:
    has_one :facebook_profile, dependent: :destroy
    has_one :twitter_profile, dependent: :destroy
    has_many :comments
    has_many :ads

  # callbacks:
    before_save :set_the_name

  # validations
    validates :profile, :presence => true

  # methods:
    # alias methods:
      def name
        def_profile.name
      end
      def nickname
        def_profile.nickname
      end
      def screen_name
        def_profile.nickname
      end
      def email
        if profile == "facebook"
          def_profile.email
        end
      end
      def image
        def_profile.image
      end
    def default_profile_link
      case self.profile
      when "twitter"
        "http://twitter.com/#{self.screen_name}"
      when "facebook"
        "http://facebook.com/#{self.screen_name}"
      end
    end
    def default_profile_display
      case self.profile
      when "twitter"
        "@#{self.screen_name}"
      when "facebook"
        "(f) #{self.screen_name}"
      end
    end
    def link_to_default_profile
      if self.profile.downcase == "facebook"
        "<a href='#{self.default_profile_link}' target='_blank'>#{self.default_profile_display}</a>"
      elsif self.profile.downcase == "twitter"
        "<a href='#{self.default_profile_link}' target='_blank'>#{self.default_profile_display}</a>"
      end
    end

    def set_the_name
      self.the_name = name + " " + nickname if def_profile
    end

    def def_profile
      if self.read_attribute(:profile) == "twitter"
        @twitter_profile ||= self.twitter_profile
      elsif self.read_attribute(:profile) == "facebook"
        @facebook_profile ||= self.facebook_profile
      else
        raise "only FB and Twitter are supported"
      end
    end

  # class methods:
    def self.create_or_update_from_omniauth( omniauth_hash )
      if omniauth_hash["provider"] == "twitter"
        profile = TwitterProfile.create_or_update( omniauth_hash )
        if profile.new_record?
          user = User.create(profile: "twitter")
          profile.user = user
          profile.save
          return user
        else
          return profile.user
        end
      elsif omniauth_hash["provider"] == "facebook"
        profile = FacebookProfile.create_or_update( omniauth_hash )
        if profile.new_record?
          user = User.create(profile: "facebook")
          profile.user = user
          profile.save
          return user
        else
          return profile.user
        end
      else
        raise "Only FB and twitter are currently supported"
      end
    end

end
