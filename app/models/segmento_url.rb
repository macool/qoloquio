class SegmentoUrl < ActiveRecord::Base
  attr_accessible :url,
                  :segmento_id

  # relationships:
    belongs_to :segmento, :counter_cache => true

  # validations:
    validates :url, :presence => true
    # validates :segmento_id, :presence => true       # gives some error.

end
