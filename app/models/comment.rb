# encoding: utf-8
include ActionView::Helpers::DateHelper

class Comment < ActiveRecord::Base
  attr_accessible :video_id,
                  # :user_id,
                  :texto,
                  :time_of_video,           # (en segundos)
                  :live

  # relations:
    belongs_to :user
    belongs_to :video

  # validations:
    validates :texto, :presence => true
    validates :video_id, :presence => true
    validates :user_id, :presence => true
    validate :not_repeated

  # callbacks:
    before_save :set_time_of_video_if_blank

  # methods:
    def prepare_js_to_sidebar
      {
        :id => id,
        :texto => texto,
        :time_ago_in_words => time_ago_in_words(created_at),
        :appears_at => appears_at,
        :user => {
          :name => user.name,
          :image => user.image,
          :default_profile_link => user.default_profile_link,
          :default_profile_display => user.default_profile_display
        }

      }
    end
    def appears_at
      time_of_video.blank? ? 0 : (time_of_video.to_i - 4)
    end
    def hashtag
      @hashtag ||= self.video.hashtag
    end
    def set_time_of_video_if_blank
      if new_record?
        (self.time_of_video = Time.now - self.video.en_vivo_desde) if self.time_of_video.blank?
      else
        (self.time_of_video = self.created_at - self.video.en_vivo_desde) if self.time_of_video.blank?
      end
    end
    def not_repeated
      if Comment.exists?(:user_id => self.user_id, :texto => self.texto, :video_id => self.video_id)
        errors.add :texto, "ya dijiste eso"
      end
    end


end
