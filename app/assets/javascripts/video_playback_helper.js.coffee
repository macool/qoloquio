window.Helpers.VideoPlaybackHelper = {
  
  isPlaying: false
  lastUpdated: null

  beginning_playtime_of_segmento: null

  timeUpdated: ->
    playtime = parseInt(this.currentTime)
    if playtime % 2 is 0 and playtime isnt window.Helpers.VideoPlaybackHelper.lastUpdated
      window.Helpers.VideoPlaybackHelper.lastUpdated = playtime
      window.Helpers.VideoPlaybackHelper.handleComments playtime
    null

  emptyPlaytimeOfSegmento: ->
    @beginning_playtime_of_segmento = null
    null

  refreshPlaytimeOfSegmento: ->
    @beginning_playtime_of_segmento = window.Helpers.VideoTimeline.getPlayTimeOnCurrentSegmento()
    null

  handleComments: ( playtime ) ->
    if @beginning_playtime_of_segmento is null
      @refreshPlaytimeOfSegmento()
    the_time = playtime + @beginning_playtime_of_segmento
    window.Helpers.VideoCommentsHelper.render_comments_until( the_time )
    null

}
