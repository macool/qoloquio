FooterViewController = {

  right_hidden: null
  left_hidden: null

  toggleLeft: (e) ->
    e.preventDefault()
    e.stopPropagation()
    self.left_hidden.slideToggle()
    unless self.right_hidden.is(":hidden")
      self.right_hidden.slideUp()

  toggleRight: (e) ->
    e.preventDefault()
    e.stopPropagation()
    self.right_hidden.slideToggle()
    unless self.left_hidden.is(":hidden")
      self.left_hidden.slideUp()

  init: ->
    @right_hidden = $("footer .right_hidden")
    @left_hidden = $("footer .left_hidden")
    $("footer .left").on "click", @toggleLeft
    $("footer .right").on "click", @toggleRight
}

jQuery ->
  FooterViewController.init()

self = FooterViewController
