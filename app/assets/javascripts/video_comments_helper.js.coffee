window.Helpers = {} unless window.Helpers

window.Helpers.VideoCommentsHelper = {
  
  $current_video: null
  $sidebar_comments: null
  comment_template: null

  video_id: null
  firstTime: true

  the_comments: []
  comments_are_sorted: false

  sortComments: ->
    @the_comments.sort ( a, b ) ->
      parseInt(a.args.appears_at) - parseInt(b.args.appears_at)
    @comments_are_sorted = true
    null

  render_comments_until: ( playtime ) ->
    unless @comments_are_sorted
      @sortComments()
    # console.log "playtime: #{playtime}"
    for comment in @the_comments
      if playtime >= comment.appears_at
        comment.renderIfNotAlready()
      else
        comment.removeIfRendered()
    window.Helpers.CommentsScrollHelper.scrollToBottomIfAlreadyAtBottom()
    @reinitializeTooltipsAndLinks()
    null

  existsCommentById: ( comment_id ) ->
    for comment in @the_comments
      if comment.id is comment_id
        return true
    false

  addComment: ( comment, auto_render = true ) ->
    unless @existsCommentById( comment.id )
      @addComments( [comment], auto_render )
    null

  addComments: ( comments, auto_render = false ) ->
    if comments.length > 0
      for comment in comments
        this_comment = new VideoComment( comment )
        if auto_render or window.Helpers.VideoPlaybackHelper.lastUpdated >= this_comment.appears_at
          this_comment.renderIfNotAlready()
          @reinitializeTooltipsAndLinks()
        @the_comments.push this_comment
    @comments_are_sorted = false
    null

  reinitializeTooltipsAndLinks: ->
    window.Helpers.LinkifyCommentsHelper.init()
    window.Helpers.Tooltip.init()
    null

  countComments: ->
    self = window.Helpers.VideoCommentsHelper
    $.get "/comments/#{self.video_id}/count", (data) ->
      if self.firstTime
        self.firstTime = false
      else
        if self.the_comments.length != parseInt(data)
          self.refreshComments()
    null

  refreshComments: ->
    if @the_comments.length > 0
      since_id = @the_comments[ @the_comments.length - 1 ].id
    else
      since_id = 0
    $.getScript("/comments/#{@video_id}?since_id=#{since_id}")
    # console.log "refreshing comments."
    null

  setCommentTemplate: ->
    @comment_template = Handlebars.compile( $("#sidebar_content_comments_template").html() )
    null

  gotDataFromWS: ( data ) ->
    self = window.Helpers.VideoCommentsHelper
    self.addComment( data, window.Helpers.VideoTimeline.is_live )
    window.Helpers.CommentsScrollHelper.scrollToBottomIfAlreadyAtBottom()
    null

  init: ->
    @$current_video = $("#current_video_playing")
    @video_id = $("#current_video_playing").data("video_id")
    @$sidebar_comments = $(".sidebar_content_comments")

    @setCommentTemplate()

    $.getScript("/comments/#{@video_id}")
    if Modernizr.websockets
      # console.log "using websockets for comments"
      @countComments()
      window.Helpers.FayeHelper.getClient().subscribe "/comments/news/#{@video_id}", @gotDataFromWS
      # setInterval(@countComments, 3000)
    else
      # console.log "using polling for comments"
      setInterval(@countComments, 3000)
    null
}

class VideoComment
  constructor: ( @args ) ->
    @id = @args.id
    @appears_at = @args.appears_at
    @rendered = false
    @html = null
    
  render: ->
    @html ||= window.Helpers.VideoCommentsHelper.comment_template( @args )

  removeFromSidebar: ->
    @rendered = false
    window.Helpers.VideoCommentsHelper.$sidebar_comments.find("#comment_view_of_comment_id_#{@id}").remove()
    null

  renderAndAppendToSidebar: ->
    window.Helpers.VideoCommentsHelper.$sidebar_comments.append @render()
    # $comment = $("#comment_view_of_comment_id_#{@id}")
    # $comment.css("margin-left", -500)
    # $comment.delay(200).animate({ marginLeft: -20 },"easeOut")
    @rendered = true
    null

  renderIfNotAlready: ->
    @renderAndAppendToSidebar() unless @rendered
    null

  removeIfRendered: ->
    @removeFromSidebar() if @rendered
    null

