window.Helpers.VideoTimelineSegmentosHelper = {

  initialized: false

  timeline_handlebar_template: null

  $timeline_content_antes:   null
  $timeline_content_ahora:   null
  $timeline_content_despues: null

  init: ->
    @timeline_handlebar_template = Handlebars.compile( $("#timeline_content_template").html() )
    @$timeline_content_antes   = $(".timeline_content_antes")
    @$timeline_content_ahora   = $(".timeline_content_ahora")
    @$timeline_content_despues = $(".timeline_content_después")
    @initialized = true
    null

  render: ( obj ) ->
    unless @initialized
      @init()

    @$timeline_content_antes.html @timeline_handlebar_template( obj.antes )
    @$timeline_content_ahora.html @timeline_handlebar_template( obj.ahora )
    @$timeline_content_despues.html @timeline_handlebar_template( obj.despues )

}

# Handlebars.registerHelper "segmento_has_links", ( segmento ) ->
#   segmento["urls"].length > 0
