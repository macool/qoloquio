window.Helpers ||= {}

window.Helpers.FlashPlayerHelper =
  initialized: false
  template: null

  templateHTML: ->
    if navigator.sayswho.isSafari()
      $("#html_player_template").html()
    else
      $("#the_flash_player_template").html()

  initialize: ->
    unless @initialized
      @initialized = true
      @template = Handlebars.compile @templateHTML()

  startFlowPlayer: ->
    unless navigator.sayswho.isSafari()
      flowplayer "the_flash_player_node", "#{@obj.hostURL}/flowplayer/flowplayer-3.2.18.swf",
        clip: { "rtmp" }
        plugins:
          rtmp:
            url: "#{@obj.hostURL}/flowplayer/flowplayer.rtmp-3.2.13.swf"

  init: (@obj) ->
    @initialize()
    selector = @obj.selector
    $("##{selector}").html @template(@obj)
    @startFlowPlayer()
    null
