window.Helpers.CommentFormHelper = {

  $form: null

  init: ->
    self = this
    @$form = $("#new_comment")
    @$form.on "submit", ->
      time_of_video = window.Helpers.VideoTimeline.getCurrentPlaytime()
      # console.log time_of_video
      self.$form.find("#comment_time_of_video").val time_of_video
      setTimeout ->
        self.$form.find(".input_new_comment").val("")
      , 300

}