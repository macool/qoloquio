window.Helpers = {} unless window.Helpers

window.Helpers.CommentTextLengthConstraint = {
  lastValue: null
  checkLength: ->
    self = window.Helpers.CommentTextLengthConstraint
    $this = $(this)
    if $this.val().length >= 250
      $this.val self.lastValue
    else
      self.lastValue = $this.val()
    null
  init: ->
    $("#comment_texto").on "keyup", @checkLength
}
