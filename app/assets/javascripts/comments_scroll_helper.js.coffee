window.Helpers.CommentsScrollHelper = {
  
  $sidebar_content_comments: null

  atBottom: true

  doTheScroll: ->
    self = window.Helpers.CommentsScrollHelper
    self.$sidebar_content_comments.animate({
      scrollTop: self.$sidebar_content_comments[0].scrollHeight
    }, 1000, "easeInCubic")

  scrollToBottom: ( force_now = false ) ->
    if force_now
      @doTheScroll()
    else
      setTimeout @doTheScroll, 500
    null  

  scrollToBottomIfAlreadyAtBottom: ->
    @scrollToBottom() if @atBottom
    null

  init: ->
    self = this
    @$sidebar_content_comments = $(".sidebar_content_comments")
    @$sidebar_content_comments.on "scroll", ->
      now_scroll = parseInt( (this.scrollHeight - this.scrollTop) / 11 )
      the_height = parseInt( parseInt(this.style.height) / 11 )
      if now_scroll == the_height
        self.atBottom = true
      else
        self.atBottom = false
    null
}
