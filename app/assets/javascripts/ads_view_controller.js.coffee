window.Helpers = {} unless window.Helpers

window.Helpers.AdsViewController = {

  ads: null
  last_ad: null

  preloadAds: ->
    $("body").append($("<div />",{ id: "preload_ads_hidden_div", class: "hidden" }))
    for ad in @ads
      ad.$node = $("<img />", { src: ad.image })
      $("#preload_ads_hidden_div").append ad.$node
    null

  getNextAd: ->
    if @last_ad == null or @last_ad >= (@ads.length - 1)
      0
    else
      @last_ad + 1

  clickedBanner: ->
    id = $(this).data("banner_id")
    $.post "/ads/#{id}/visited", {"_method":"put"}
    null

  changeAd: ->
    @last_ad = @getNextAd()
    new_ad = @ads[@last_ad]
    $.post("/ads", { id: new_ad.id })
    @$div.html($("<a />", {
      "data-banner_id": new_ad.id,
      class: "banner_ad_link",
      target: "_blank",
      html: new_ad.$node,
      href: new_ad.url
    }))
    setTimeout( ->
      self.changeAd()
    , (new_ad.display_time * 1000))
    null

  init: ->
    $(document).on("click", ".banner_ad_link", @clickedBanner)
    @$div = $("#the_ads")
    $.getJSON "/ads", (data) ->
      unless data.length is 0
        self.ads = data
        self.preloadAds()
        self.changeAd()
    null

}

self = window.Helpers.AdsViewController
