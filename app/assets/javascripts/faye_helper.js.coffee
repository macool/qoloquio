
window.Helpers.FayeHelper = {

  faye: null

  initialized: false

  init: ->
    host = "http://" + window.location.hostname + ":9292"
    @faye = new Faye.Client("#{host}/faye")
    @initialized = true
    null

  getClient: ->
    @init() unless @initialized
    @faye

}
