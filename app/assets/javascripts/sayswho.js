navigator.sayswho = {
  isSafari: function () {
    return navigator.sayswho.browser.indexOf("Safari") > -1;
  },
  isiOS: function () {
    return /(iPad|iPhone|iPod)/g.test( navigator.userAgent );
  }
};

navigator.sayswho.browser = (function(){
    var ua= navigator.userAgent, tem,
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*([\d\.]+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+(\.\d+)?)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    M= M[2]? [M[1], M[2]]:[navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/([\.\d]+)/i))!= null) M[2]= tem[1];
    return M.join(' ');
})();
