window.Helpers.VideoTimeline = {

  now_showing_on_tl: "ahora"
  interval_id: null
  video_id: null
  is_live: null

  segmentos: []
  now_playing: null
  live_html_cache: null

  timeline_handlebar_template: null

  getCurrentPlaytime: ->
    @getPlayTimeOnCurrentSegmento() + $("video")[0].currentTime

  getPlayTimeOnCurrentSegmento: ->
    @getPlaytimeOnSegmentoByIndex @now_playing

  getPlaytimeOnSegmentoById: ( id ) ->
    @getPlaytimeOnSegmentoByIndex @findIndexOfSegmentoById(id)

  getPlaytimeOnSegmentoByIndex: ( index ) ->
    playtime = 0
    for segmento in @segmentos.slice(0, index)
      playtime += segmento.duracion
    playtime

  findIndexOfSegmentoById: ( id ) ->
    for i in [0..@segmentos.length]
      if @segmentos[i].id is id
        return i

  wantsToPlaySegmento: ( e ) ->
    e.preventDefault()
    e.stopPropagation()
    $this = $(this)
    segmento_id = $this.data("segmento_id")
    self.now_playing = self.findIndexOfSegmentoById( segmento_id )
    self.doPlaySegmento()

  doPlaySegmento: ->
    segmento = self.segmentos[self.now_playing]
    video_sources = ""
    for url in segmento.urls
      video_sources += "<source src='#{url}'>"
    video_node = $("<video />", {
      width: segmento.width,
      height: segmento.height,
      controls: "controls",
      autoplay: "autoplay",
      "data-segmento_index": self.now_playing,
      html: video_sources
    }).on("ended", self.videoFinished)
    if self.is_live and self.live_html_cache is null
      self.live_html_cache = $("#current_video_playing").html()
    unless self.is_live
      video_node.on("timeupdate", window.Helpers.VideoPlaybackHelper.timeUpdated)
      window.Helpers.VideoPlaybackHelper.lastUpdated = null
      self.handleTl()
    window.Helpers.VideoPlaybackHelper.emptyPlaytimeOfSegmento()
    $("#current_video_playing").html video_node
    $("#current_video_playing").append $("<div />", {
      html: segmento.titulo,
      class: "white_text"
    })
    null

  handleTl: ->
    window.Helpers.VideoTimelineSegmentosHelper.render({
      antes:   self.segmentos.slice( 0, self.now_playing ),
      ahora:   [ self.segmentos[self.now_playing] ],
      despues: self.segmentos.slice( (self.now_playing + 1), self.segmentos.length )
    })
    null

  wantsToSeeLiveIfNotAlready: ->
    if @now_playing is null
      # console.log "already showing live"
    else
      # console.log "not showing live"
      $("#current_video_playing").html @live_html_cache
      @live_html_cache = null
      @now_playing = null
    null

  clickedToggleButton: ->
    $this = $(this)
    unless $this.text() is self.now_showing_on_tl
      self.now_showing_on_tl = $this.text()
      $(".timeline_content > div").addClass("hidden")
      $(".timeline_content_#{self.now_showing_on_tl}").removeClass("hidden")
    if $this.text() is "ahora" and self.is_live
      self.wantsToSeeLiveIfNotAlready()
    null

  videoFinished: ->
    # console.debug "finished"
    unless self.now_playing >= (self.segmentos.length - 1)
      self.now_playing += 1
      self.doPlaySegmento()
    else
      if self.is_live
        $("#timeline_toggle_ahora_button").trigger "click"
    null

  setListeners: ->
    $(".timeline_toggle button").on "click", @clickedToggleButton
    $(document).on "click", ".tl_segmento_nombre_playing", @wantsToPlaySegmento
    null

  refreshTimeLine: ->
    if self.is_live
      request_string = "/videos/#{self.video_id}/tl"
      $.getScript request_string
    if @segmentos.length is 0 or @is_live
      $.getJSON "/videos/#{@video_id}/tl", (data) ->
        self.segmentos = data
        if not self.is_live and self.segmentos.length > 0 and self.now_playing is null
          self.now_playing = 0
          self.doPlaySegmento()
    null

  beginInterval: ->
    self.refreshTimeLine()
    if self.is_live
      @interval_id = setInterval ->
        self.refreshTimeLine()
      , 45000
    null

  init: ( video_id ) ->
    @video_id = video_id
    @is_live = $("#current_video_playing").data("live_video")
    @setListeners()
    @beginInterval()
    null
}

self = window.Helpers.VideoTimeline
