class window.Qoloquio
  constructor: (autoplay = true) ->
    @safari = navigator.sayswho.isSafari()
    @autoplay() if autoplay
    @removeHiddenClassIfIos()

  removeHiddenClassIfIos: ->
    if navigator.sayswho.isiOS()
      $(".qoloquio-hidden").removeClass "qoloquio-hidden"

  autoplay: ->
    if @safari
      @play()
    else
      @player().onLoad =>
        @setVolume 100

  player: ->
    @_player ||= if @safari
      document.getElementById("qoloquio_player")
    else
      $f()

  setVolume: (volume) ->
    if @safari
      @player().volume = volume / 100
    else
      @player().setVolume volume

  play: ->
    @player().play()

  stop: ->
    @player().stop()
