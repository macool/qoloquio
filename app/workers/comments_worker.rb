class CommentsWorker
  include Sidekiq::Worker

  sidekiq_options queue: "qoloquio_comments"

  def perform(comment_id)
    comment = Comment.find(comment_id, :include => :user)

    # broadcast to sockets:
    message = {
      :channel => "/comments/news/#{comment.video_id}",
      :data => comment.prepare_js_to_sidebar,
      :ext => { :auth_token => FAYE_TOKEN }
    }
    uri = URI.parse("http://localhost:9292/faye")
    Net::HTTP.post_form(uri, :message => message.to_json)

    unless Rails.env == "development"
      # share to social networks:
      case comment.user.profile
      when "twitter"
        # cut tweet, and tweet.
        text = "#{comment.hashtag} "
        text += comment.texto.slice(0, 80) + ".. http://qoloquio.com"
        text += comment.live ? "" : "/videos/#{comment.video_id}"
        Twitter.configure do |config|
          config.oauth_token = comment.user.twitter_profile.token_access
          config.oauth_token_secret = comment.user.twitter_profile.token_secret
        end
        if comment.user.twitter_profile.share({:message => text})
          comment.shared = true
          comment.save
        end
      when "facebook"
        text = "#EleccionesEC "
        text += comment.texto
        link = "http://qoloquio.com" + ( comment.live ? "" : "/videos/#{comment.video_id}" )
        text += " " + link
        if comment.user.facebook_profile.share({:message => text, :link => link})
          comment.shared = true
          comment.save
        end
      end
    end
  end
  
  
end