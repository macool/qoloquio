module ApplicationHelper

  def form_errors( instance )
    if instance.errors.any?
      html = '<div class="errors alert alert-error">'
        html += '<ul>'
          instance.errors.full_messages.each do |message|
            html += "<li>#{message}</li>"
          end
        html += '</ul>'
      html += '</div>'
    end
    raw html
  end

  def broadcast( channel, data )
    message = {
      :channel => channel,
      :data => data,
      :ext => { :auth_token => FAYE_TOKEN }
    }
    uri = URI.parse("http://localhost:9292/faye")
    Net::HTTP.post_form(uri, :message => message.to_json)
  end

  def full_host_url
    "#{request.protocol}#{request.host_with_port}"
  end
end
