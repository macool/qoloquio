OMNIAUTH_CREDENTIALS = YAML.load(File.open(Rails.root.join('config', 'omniauth_credentials.yml')))

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, OMNIAUTH_CREDENTIALS["facebook"]["access"], OMNIAUTH_CREDENTIALS["facebook"]["secret"], :scope => 'email,user_birthday,user_about_me,publish_stream,publish_actions,user_likes,offline_access'
  provider :twitter, OMNIAUTH_CREDENTIALS["twitter"]["access"], OMNIAUTH_CREDENTIALS["twitter"]["secret"]
end

# twitter intializer: (deprecated)
# Twitter.configure do |config|
#   config.consumer_key = options["twitter"]["access"]
#   config.consumer_secret = options["twitter"]["secret"]
# end

# facebook initializer:
# (doesnt exist yet)
