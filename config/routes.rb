require 'sidekiq/web'
require "admin_constraint"

Qoloquio::Application.routes.draw do

  match '/auth/:provider/callback' => 'users#create' # omniauth
  get "auth/failure" => "home#auth_failure"

  get '/logout' => 'sessions#destroy', as: 'logout'

  # admin path:
  get 'admin' => 'admin#index'

  resources :embedded, only: [:show]

  resources :comments do
    member do
      get :count
    end
  end

  resources :ads do
    member do
      put :visited
    end
  end

  resources :videos do
    member do
      get :tl
    end
  end

  namespace :admin do
    resources :videos, path: "programas" do
      member do
        get :embed
      end
    end
    resources :ads
    resources :users do
      member do
        get :admin
        get :ban
      end
    end
    resources :preferences
    resources :segmentos
  end

  # sidekiq:
  mount Sidekiq::Web, at: "/sidekiq_web", :constraints => AdminConstraint.new

  root to: 'home#index'

  match '*a', :to => 'application#routing_error'
end
