class AddWidthAndHeigthToSegmento < ActiveRecord::Migration
  def up
    change_table :segmentos do |t|
      t.integer :width, :default => 608
      t.integer :height, :default => 368
    end
  end
  def down
    change_table :segmentos do |t|
      t.remove :width
      t.remove :height
    end
  end
end
