class MakeFacebookProfileFieldsImportant < ActiveRecord::Migration
  def up
    add_index :facebook_profiles, :uid, :unique => true
    add_index :facebook_profiles, :email, :unique => true
    add_index :facebook_profiles, :token, :unique => true
    add_index :facebook_profiles, :user_id
  end

  def down
    remove_index :facebook_profiles, :uid, :unique => true
    remove_index :facebook_profiles, :email, :unique => true
    remove_index :facebook_profiles, :token, :unique => true
    remove_index :facebook_profiles, :user_id
  end
end
