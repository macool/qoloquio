class AddTypeToVideo < ActiveRecord::Migration
  def change
    add_column :videos, :kind, :string, default: "Video"
    add_index  :videos, :kind
  end
end
