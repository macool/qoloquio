class FixFieldsOnSegmentos < ActiveRecord::Migration
  def up
    change_table :segmentos do |t|
      t.remove :desde, :hasta
      t.integer :duracion
    end
  end

  def down
    change_table :segmentos do |t|
      t.datetime :desde
      t.datetime :hasta
      t.remove :duracion
    end
  end
end
