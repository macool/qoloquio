class CreateSegmentoUrls < ActiveRecord::Migration
  def change
    create_table :segmento_urls do |t|

      t.string :url, :null => false
      t.references :segmento, :null => false

      t.timestamps
    end

    add_index :segmento_urls, :segmento_id

  end
end
