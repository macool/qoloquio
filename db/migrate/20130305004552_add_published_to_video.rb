class AddPublishedToVideo < ActiveRecord::Migration
  def change
    add_column :videos, :published, :boolean, :default => false
    add_index :videos, :published
  end
end
