class CreatePreferences < ActiveRecord::Migration
  def change
    create_table :preferences do |t|
      t.string :key, :null => false
      t.text :value, :null => false
      t.text :descripcion
      t.timestamps
    end
    add_index :preferences, :key, :unique => true
  end
end
