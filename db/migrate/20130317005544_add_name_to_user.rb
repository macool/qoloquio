class AddNameToUser < ActiveRecord::Migration
  def change
    add_column :users, :the_name, :string
    add_index :users, :the_name
  end
end
