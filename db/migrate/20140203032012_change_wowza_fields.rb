require "video_poster_uploader"
require "video"

class ChangeWowzaFields < ActiveRecord::Migration
  def up
    change_table :videos do |t|
      t.string :wowza_server
      t.string :wowza_app, default: "live"
      t.string :wowza_channel
    end
    Video.find_each do |video|
      unless video.wowza_file_url.blank?
        uris = video.wowza_file_url.split("rtmp://").last.split("/")
        wowza_fields = {
          wowza_server: uris.shift,
          wowza_app: uris.shift,
          wowza_channel: uris.shift.split(":").last
        }
        video.update_attributes! wowza_fields
      end
    end
    change_table :videos do |t|
      t.remove :wowza_file_url
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
