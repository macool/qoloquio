class RemoveUrlFromVideo < ActiveRecord::Migration
  def up
    change_table :segmentos do |t|
      t.remove :url
    end
  end

  def down
    change_table :segmentos do |t|
      t.string :url
    end
  end
end
