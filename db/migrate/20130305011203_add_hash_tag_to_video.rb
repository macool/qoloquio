class AddHashTagToVideo < ActiveRecord::Migration
  def change
    add_column :videos, :hashtag, :string, :default => "#qoloquio"
    add_index :videos, :hashtag
  end
end
