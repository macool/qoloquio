class CreateTwitterProfiles < ActiveRecord::Migration
  def change
    create_table :twitter_profiles do |t|

      t.string :uid, :null => false
      t.string :nickname, :null => false
      t.string :name
      t.string :description
      t.string :location
      t.string :image
      t.text :urls

      t.string :token_access, :null => false
      t.string :token_secret, :null => false

      t.text :raw_info

      t.references :user

      t.timestamps
    end

    add_index :twitter_profiles, :uid, :unique => true
    add_index :twitter_profiles, :user_id, :unique => true
    add_index :twitter_profiles, :token_access, :unique => true
    add_index :twitter_profiles, :token_secret, :unique => true

  end
end
