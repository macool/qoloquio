class AllowBanningUsers < ActiveRecord::Migration
  def up
    change_table :users do |t|
      t.boolean :banned, :default => false
    end
  end

  def down
    change_table :users do |t|
      t.remove :banned
    end
  end
end
