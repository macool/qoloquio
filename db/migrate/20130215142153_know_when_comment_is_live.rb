class KnowWhenCommentIsLive < ActiveRecord::Migration
  def up
    change_table :comments do |t|
      t.boolean :live, :default => false
    end
  end

  def down
    change_table :comments do |t|
      t.remove :live
    end
  end
end
