class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|

      t.references :user
      t.references :video

      t.string :texto

      t.integer :time_of_video, :limit => 8

      t.timestamps
    end
  end
end
