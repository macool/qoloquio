class CreateSegmentos < ActiveRecord::Migration
  def change
    create_table :segmentos do |t|

      t.references :video, :null => false
      t.string :titulo
      t.string :url

      t.datetime :desde
      t.datetime :hasta

      t.timestamps
    end

    add_index :segmentos, :video_id

  end
end
