class CreateFacebookProfiles < ActiveRecord::Migration
  def change
    create_table :facebook_profiles do |t|

      # main info:
      t.string :uid, null: false
      t.string :nickname
      t.string :email
      t.string :name
      t.string :first_name
      t.string :last_name
      t.string :image
      t.text :urls
      t.string :location
      t.boolean :verified

      # credentials:
      t.string :token
      t.string :token_expires_at
      t.boolean :token_expires

      # extra info:
      t.text :raw_info

      # association:
      t.references :user

      t.timestamps
    end
  end
end
