class KnowIfCommentShared < ActiveRecord::Migration
  def up
    change_table :comments do |t|
      t.boolean :shared, :default => false
    end
  end

  def down
    change_table :comments do |t|
      t.remove :shared
    end
  end
end
