class DoCounterCacheOnSegmentoUrls < ActiveRecord::Migration

  class Segmento < ActiveRecord::Base
    attr_accessible :segmento_urls_count
    has_many :segmento_urls
  end

  class SegmentoUrl < ActiveRecord::Base
    belongs_to :segmento, :counter_cache => true
  end

  def up
    Segmento.find_each do |segmento|
      segmento.update_attribute :segmento_urls_count, segmento.segmento_urls.count
    end
  end

  def down
  end
end
