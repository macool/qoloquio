class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|

      t.string :profile, :null => false

      t.timestamps
    end
  end
end
