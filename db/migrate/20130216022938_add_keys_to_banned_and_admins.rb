class AddKeysToBannedAndAdmins < ActiveRecord::Migration
  def change
    add_index :users, :banned
    add_index :users, :is_admin
  end
end
