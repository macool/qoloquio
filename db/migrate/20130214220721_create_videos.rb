class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :poster
      t.text :iframe_code
      t.datetime :en_vivo_desde
      t.datetime :en_vivo_hasta
      t.string :titulo
      t.text :descripcion
      t.timestamps
    end
  end
end
