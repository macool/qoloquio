class AddWowzaVersionToVideos < ActiveRecord::Migration
  def change
    add_column :videos, :wowza_file_url, :string
    add_column :videos, :wowza_file_height, :integer, :default => 480
    add_column :videos, :wowza_file_width, :integer, :default => 640
  end
end
