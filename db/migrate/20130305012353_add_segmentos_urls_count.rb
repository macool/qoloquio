class AddSegmentosUrlsCount < ActiveRecord::Migration
  def up
    add_column :segmentos, :segmento_urls_count, :integer, :default => 0
  end

  def down
    remove_column :segmentos, :segmento_urls_count
  end
end
