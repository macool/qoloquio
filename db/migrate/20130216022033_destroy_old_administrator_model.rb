class DestroyOldAdministratorModel < ActiveRecord::Migration
  def up
    drop_table :administrators

    change_table :users do |t|
      t.boolean :is_admin, :default => false
    end

  end

  def down
    change_table :users do |t|
      t.remove :is_admin
    end

    create_table :administrators do |t|
      t.integer  :user_id
      t.timestamps
    end

    add_index :administrators, :user_id, :unique => true
  end
end
