class AddIndexToAdministrator < ActiveRecord::Migration
  def change
    add_index :administrators, :user_id, :unique => true
  end
end
