class CreateAds < ActiveRecord::Migration
  def change
    create_table :ads do |t|

      t.datetime :desde
      t.datetime :hasta

      t.integer :clicks, :default => 0
      t.integer :times_displayed, :default => 0

      t.string :image

      t.references :user

      t.timestamps
    end
    add_index :ads, :desde
    add_index :ads, :hasta
    add_index :ads, :user_id
  end
end
